import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    result = []
    for i in ['Mr.', 'Mrs.', 'Miss.']:
        result.append((i, 
                       df.loc[df['Name'].str.contains(i)]['Age'].isna().sum(), 
                       int(df.loc[df['Name'].str.contains(i)]['Age'].median())))
    return result
